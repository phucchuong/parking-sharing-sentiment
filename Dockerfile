from python:3.7

#ENV HTTP_PROXY "http://10.30.176.10:6776"
#ENV HTTPS_PROXY "http://10.30.176.10:6776"

RUN mkdir /sentiment
WORKDIR /sentiment

COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY . .

ENTRYPOINT ["python"]
CMD ["app.py"]