from flask import Flask, request
predict_app = Flask(__name__)

from src.predict import test_predict, predict

@predict_app.route('/')
def home():
    return "OK"

@predict_app.route('/classification', methods=['POST'])
def predict_from_json():
    json_data = request.json
    data = [] 
    for dt in json_data['data']:
        data.append(dt)
    ans = predict(data)
    return ans

@predict_app.route('/classification', methods=["GET"])
def predict_from_query():
    sentence = request.args.get('sentence')
    ans = predict([{'review':sentence}])
    return {'result': list(ans.keys())[0]}

if __name__ == "__main__":
    predict_app.run(host='0.0.0.0')