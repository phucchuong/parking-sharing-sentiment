from underthesea.word_tokenize import word_tokenize
import re
try:
    from utils.file_helpers import get_acronym_list
except:
    from file_helpers import get_acronym_list

list_acronym = get_acronym_list()
#print(list_acronym)

def normalize_text(text, replace_acronym):   
    text = " "+text.lower()+ " "
    text = re.sub(r'(.)\1+', lambda m: m.group(1), text)
    text = re.sub(r'(\W)', ' ', text)
    if replace_acronym:
        #print(text)
        for k in list_acronym:
            text = text.replace(" "+k+" ", " "+list_acronym[k]+" ")
        #print(text)
    #print(text)
    return text

def tokenize_text(text):
    output = word_tokenize(text, format='text')
    return output

if __name__ == "__main__":   
    print(list_acronym)