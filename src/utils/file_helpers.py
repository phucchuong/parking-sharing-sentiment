import os.path as p
from pathlib import Path
import pandas as pd
from joblib import load, dump

def __make_file_path(path):
    folder = Path(p.dirname(__file__))
    return folder.parent / path

def save_model(model, path):
    fpath = __make_file_path(path)
    dump(model, fpath)
    print("SAVED !!!")

def load_model(path):
    fpath = __make_file_path(path)
    return load(fpath)

def get_data_from_txt_file(path):
    fpath = __make_file_path(path)
    f = open(fpath, encoding='utf8')
    return f.readlines()

def get_acronym_list(path='../data/acronym_words.txt'):
    data = get_data_from_txt_file(path)
    list_acronym = {}
    for line in data:
        t = line.split(',')
        list_acronym[t[0]] = t[1].strip()
    return list_acronym

def get_stop_word_list(path='../data/stop_words.txt'):
    data = get_data_from_txt_file(path)
    list_stop_words = []
    for line in data:
        list_stop_words.append(line.strip())
    return list_stop_words

def get_data_from_csv(path):
    fpath = __make_file_path(path)
    return pd.read_csv(fpath)

def get_data_from_excel(path):
    fpath = __make_file_path(path)
    return pd.read_excel(fpath, index_col=0)

if __name__ == "__main__":
    print(get_acronym_list())
    print(get_stop_word_list())