import sys
sys.path.append("src")

from utils.file_helpers import load_model, get_data_from_csv, get_data_from_excel
import pandas as pd
import numpy as np

clf_sentiment = load_model('../model/sentiment.joblib')
clf_service_app = load_model('../model/service_app.joblib')
label = {0:'pos - app', 1:'pos - service', 2:'neg - app', 3:'neg - service'}

def cal_statistic(sentiment, service_app):
    sen_arr = np.array(sentiment)
    ser_arr = np.array(service_app)
    x = sen_arr*2 + ser_arr
    unique, counts = np.unique(x, return_counts=True)
    statistic = dict(zip(unique, counts))
    return {label[x]:int(statistic[x]) for x in statistic}

def predict(json_data):
    try:
        data = pd.DataFrame(json_data)
        #createAt = data['createAt']
        reviews = data['review']
        reviews = reviews.replace(np.nan, '', regex=True)
        sentiment = clf_sentiment.predict(reviews)
        service_app = clf_service_app.predict(reviews)
        return cal_statistic(sentiment, service_app)
    except :
        raise ValueError("invalid data")

def print_result(text, ans_sentiment, ans_service_app):
    for t, s, sa in zip(text, ans_sentiment, ans_service_app):
        ans = ""
        ans += "Pos / " if s == 0 else "Neg / "
        ans += "Service" if sa == 1 else "App"
        print(ans, t)  
    print(cal_statistic(ans_sentiment, ans_service_app))

def test_predict(texts):
    sentiment = clf_sentiment.predict(list(texts))
    service_app = clf_service_app.predict(list(texts))
    print_result(list(texts), sentiment, service_app)

if __name__ == "__main__":    
    #data = get_data_from_excel('../data/test.xlsx')
    data = [
        'Tôi cài đặt lần đầu và đã đăng ký hoàn tất để sử dụng ứng dụng. Tuy nhiên khi đăng nhập, '
        +'ứng dụng báo tôi không đăng nhập được và yêu cầu gửi request? Tôi không hiểu request của cái gì ở đây? '
        +'Sau một hồi nhập đi nhập lại không được tôi ấn quên mật khẩu để set up lại thì lại báo không tìm tháy '
        +'số điện thoại được đăng ký? Tôi tiếp tục vẫn dùng số đấy để đăng ký mới lại hoàn toàn, ứng dụng lại báo số của tôi đã có trong kho lưu trữ?',
        'Trải nghiệm thú vị với myparking, ứng dụng tiện ích, đồ họa đẹp',
        'App có cải tiến,Đã dùng ngon hơn so với phiên bản cũ.Mong team tiếp tục phát triển để thuận tiện hơn',
        'còn ít địa điểm gửi xe quá',
        'ngon lành cành đào',
        "ứng dụng hay quá",
        "ap quá hay",
        'không thể chê vào đâu được',
        'gửi xe mà giá tận 25k, quá mắc',
        'kết nối quá chậm',
        'không đăng nhập được',
        'nhân viên phục vụ quá chán'
    ]
    result = test_predict(data)