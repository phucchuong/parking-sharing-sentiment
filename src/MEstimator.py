from sklearn.base import TransformerMixin, BaseEstimator
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from utils.text_utils import tokenize_text, normalize_text

class MTokenizer(BaseEstimator, TransformerMixin):
    def __tokenize_data_set(self, texts):
        output = []    
        for text in texts:
            output.append(tokenize_text(text))
        return output

    def fit(self, X, y=None):
        self.fit_transform(X)
        return self

    def fit_transform(self, texts, y=None):
        return self.transform(texts)
        
    def transform(self, texts):
        return self.__tokenize_data_set(texts)
    
class MTextNormalize(BaseEstimator, TransformerMixin):
    def __init__(self, replace_acronym = True):
        self._replace_acronym = replace_acronym

    @property
    def replace_acronym(self):
        return self._replace_acronym     
    @replace_acronym.setter
    def replace_acronym(self, value):
        self._replace_acronym = value

    def __normalize_data_set(self, texts):
        output = []    
        for text in texts:
            output.append(normalize_text(text, self._replace_acronym))
        return output

    def fit_transform(self, X, y=None):       
        return self.__normalize_data_set(X)
    
    def fit(self, X, y=None):
        self.fit_transform(X)       
        return self

    def transform(self, X):
        return self.__normalize_data_set(X)

class MVectorize(BaseEstimator, TransformerMixin):
    def __init__(self, stop_words, min_df=1, max_df=1.0, max_features=10000):
        self._list_stop_words = stop_words
        self._min_df = min_df
        self._max_df = max_df
        self._max_features = max_features
        self._vectorizer = self.__getTfidfVectorizer()

    def __getTfidfVectorizer(self):
        return TfidfVectorizer(stop_words=self._list_stop_words, min_df=self._min_df, 
                                max_df=self._max_df, max_features=self._max_features)

    @property
    def stop_words(self):
        return self._stop_words
    @stop_words.setter
    def stop_words(self, value):
        self._stop_words = value
        self._vectorizer = self.__getTfidfVectorizer()

    @property
    def min_df(self):
        return self._min_df
    @min_df.setter
    def min_df(self, value):
        self._min_df = value
        self._vectorizer = self.__getTfidfVectorizer()

    @property
    def max_features(self):
        return self._max_features
    @max_features.setter
    def max_features(self, value):
        self._max_features = value
        self._vectorizer = self.__getTfidfVectorizer()

    @property
    def max_df(self):
        return self._max_df
    @max_df.setter
    def max_df(self, value):
        self._max_df = value
        self._vectorizer = self.__getTfidfVectorizer()

    def fit(self, X, y=None):
        return self._vectorizer.fit(X)
    def fit_transform(self, X, y=None):        
        X = self._vectorizer.fit_transform(X)
        feature_names = self._vectorizer.get_feature_names()
        print(feature_names, len(feature_names))
        return X
    def transform(self, X):
        return self._vectorizer.transform(X)



class Test(BaseEstimator, TransformerMixin):
    def __init__(self, texts):
        print(texts)
    def fit(self, X, y=None):
        print(X, y)
        return self


from sklearn.pipeline import Pipeline
if __name__ == "__main__":
    t = Test("test instance")
    m = MTokenizer()
    pipe = Pipeline([
        ('p1', MTokenizer()),
        ('p2', Test("test"))
    ])

    data = [
        "Chất lượng sản phẩm tuyệt vời Đóng gói sản phẩm rất đẹp và chắc chắn Shop phục vụ rất tốt Rất đáng tiền Rất đáng tiền. Giao hàng hôm sau là có",
        "Toàn hàng trungkhi mua quên ko coi kĩ",
        "Đóng gói sản phẩm rất đẹp và chắc chắn. Được shop tặng thêm quà nữa"
    ]

    pipe.fit(data,[1])
